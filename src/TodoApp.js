import React, { useState } from 'react';
import Header from './Header';
import TodoList from './TodoList';
import AddTodoForm from './AddTodoForm';
import initialTodos from './initialTodos';
import { v4 as uuidv4 } from 'uuid';
import './TodoApp.styles.css';

function TodoApp() {

  const [todos, setTodos] = useState(initialTodos);

  const handleAddTodo = (todoContent) => {
    if(todoContent){
      setTodos([...todos, {
        id: uuidv4(),
        content: todoContent,
        completed: false
      }]);
    }
  }

  const handleRemoveTodo = (deleteTodo) => {
    const newList = todos.filter( (todo)=>(todo.id !== deleteTodo.id));
    setTodos(newList);
  }

  return (
    <div>
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
      <Header />
      <main className="content-wrapper">
        <TodoList todos={todos} deleteTodo={handleRemoveTodo} />
        <AddTodoForm addTodo={handleAddTodo} />
      </main>
    </div>
  );
}

export default TodoApp;
