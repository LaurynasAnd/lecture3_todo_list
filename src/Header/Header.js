import React from 'react';
import './Header.styles.css';

export default function Header() {
    
    return (
        <header>
            <h1>Todo List</h1>
        </header>
    );
}