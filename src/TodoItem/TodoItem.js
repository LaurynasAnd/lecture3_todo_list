import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Box, ListItem } from '@material-ui/core';
import './TodoItem.styles.css';

export default function TodoItem({todo, deleteTodo}){
    const [completed, setCompleted] = useState(todo.completed);


    const handleCompletedTask = ()=> setCompleted(!completed);

    const handleDeleteTodo = () => deleteTodo(todo);

    return (
        <ListItem component="li" className={"todo-item " + completed}>
            {todo.content}
            <div className="buttons">
                { !completed && <Button className="complete"
                                        variant="outlined"
                                        size="small"
                                        onClick={handleCompletedTask}>
                                            Complete
                                </Button>}
                { completed && <Button  className="revert"
                                        variant="outlined" 
                                        size="small"
                                        onClick={handleCompletedTask}>
                                            Revert
                                </Button>}
                <Button className="delete" 
                        variant="outlined"
                        size="small" 
                        onClick={handleDeleteTodo}>
                            Delete
                </Button>
            </div>
        </ListItem>
    );
}

TodoItem.propTypes = {
    todo: PropTypes.object,
    deleteTodo: PropTypes.func
  };