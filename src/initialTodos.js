import { v4 as uuidv4 } from 'uuid';

const initialTodos = [
  { id: uuidv4(), content: 'Create page structure', completed: false },
  { id: uuidv4(), content: 'Add some styles', completed: false }, 
  { id: uuidv4(), content: 'Add dynamic functionality', completed: false }
]

export default initialTodos;
