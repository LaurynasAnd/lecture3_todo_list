import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, TextField } from '@material-ui/core';
import './AddTodoForm.styles.css';

export default function AddTodoForm({addTodo}){

    const [inputValue, setInputValue] = useState("");

    const handleInputChange = (e) => setInputValue(e.target.value);

    const handleSubmit = (e) => {
        e.preventDefault();
        addTodo(inputValue);
        setInputValue("");
    } 

    return(
        <form  className="add-todo-form" onSubmit={handleSubmit}>
            <TextField  id="standard-basic" 
                        label="What needs to be done"
                        size="small"
                        type="text" 
                        onChange={handleInputChange}
                        value={inputValue} />
            <Button className="add-todo" 
                    variant="outlined"
                    size="small"
                    type="submit">
                        Add todo
            </Button>
        </form>
    );
}
AddTodoForm.propTypes = {
    addTodo: PropTypes.func
  };