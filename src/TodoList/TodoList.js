import React from 'react';
import PropTypes from 'prop-types';
import { List } from '@material-ui/core';
import TodoItem from '../TodoItem';
import './TodoList.styles.css';

export default function TodoList ({todos, deleteTodo}){
    return (
        <List component="ul" className="todo-list">
            {
                todos.map(todo => <TodoItem key={todo.id} todo={todo} deleteTodo={deleteTodo} />)
            }
        </List>
    );
}

TodoList.propTypes = {
    todos: PropTypes.array,
    deleteTodo: PropTypes.func
  };